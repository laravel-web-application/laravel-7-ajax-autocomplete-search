<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 10; $i++)
            DB::table('items')->insert([
                'name' => 'Laravel ' . $i++,
                'created_at' => Carbon::now(),
            ]);
    }
}
